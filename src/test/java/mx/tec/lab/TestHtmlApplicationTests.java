package mx.tec.lab;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlParagraph;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class TestHtmlApplicationTests {
	private WebClient webClient;
	
	@BeforeEach
	public void init() throws Exception{
		webClient = new WebClient();
	}
	
	@AfterEach
	public void close() throws Exception{
		webClient.close();
	}
	
	
	public void givenAClient_whenEnteringAutomationPractice_thenPageTitleIsCorrect()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php");
			
			assertEquals("My Store", page.getTitleText());
	}
	
	
	public void givenAClient_whenEnteringLoginCredentials_thenAccountPageIsDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php?controller=authentication&back=my-account");
			HtmlTextInput emailField = (HtmlTextInput) page.getElementById("email");
			emailField.setValueAttribute("carlos.estradaih@gmail.com");
			HtmlPasswordInput passwordField = (HtmlPasswordInput) page.getElementById("passwd");
			passwordField.setValueAttribute("Contrasenia");
			HtmlButton submitButton = (HtmlButton) page.getElementById("SubmitLogin");
			HtmlPage resultPage = submitButton.click();
			
			assertEquals("My account - My Store", resultPage.getTitleText());
	}
	
	
	public void givenAClient_whenEnteringWrongLoginCredentials_thenAuthenticationPageIsDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php?controller=authentication&back=my-account");
			HtmlTextInput emailField = (HtmlTextInput) page.getElementById("email");
			emailField.setValueAttribute("carlos.estradaih@hotmail.com");
			HtmlPasswordInput passwordField = (HtmlPasswordInput) page.getElementById("passwd");
			passwordField.setValueAttribute("Contrasenio");
			HtmlButton submitButton = (HtmlButton) page.getElementById("SubmitLogin");
			HtmlPage resultPage = submitButton.click();
			
			assertEquals("Login - My Store", resultPage.getTitleText());
	}
	
	@Test
	public void givenAClient_whenEnteringWrongLoginCredentials_thenErrorMessageIsDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php?controller=authentication&back=my-account");
			HtmlTextInput emailField = (HtmlTextInput) page.getElementById("email");
			emailField.setValueAttribute("carlos.estradaih@hotmail.com");
			HtmlPasswordInput passwordField = (HtmlPasswordInput) page.getElementById("passwd");
			passwordField.setValueAttribute("Contrasenio");
			HtmlButton submitButton = (HtmlButton) page.getElementById("SubmitLogin");
			HtmlPage resultPage = submitButton.click();
			HtmlListItem result = resultPage.getFirstByXPath("//div[@class='alert alert-danger']/ol/li");
			assertEquals("Authentication failed.", result.getTextContent());
	}
	
	
	public void givenAClient_whenSearchingNotExistingProduct_thenNoResultsDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php");
			HtmlTextInput buscador = (HtmlTextInput) page.getElementById("search_query_top");
			buscador.setValueAttribute("camisas");
			HtmlButton buscar = (HtmlButton) page.getElementByName("submit_search");
			HtmlPage resultPage = buscar.click();
			HtmlSpan result = resultPage.getFirstByXPath("//span[@class = 'heading-counter']");
			assertEquals("0 results have been found.", result.getTextContent().trim());
	}
	
	
	public void givenAClient_whenSearchingEmptyString_thenPleaseEnterDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php");
			HtmlButton buscar = (HtmlButton) page.getElementByName("submit_search");
			HtmlPage resultPage = buscar.click();
			HtmlParagraph result = resultPage.getFirstByXPath("//p[@class= 'alert alert-warning']");
			assertEquals("Please enter a search keyword", result.getTextContent().trim());
		}
	
	
	public void givenAClient_whenSigningOut_thenAuthenticationPageIsDisplayed()
		throws Exception{
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			HtmlPage page = webClient.getPage("http://automationpractice.com/index.php?controller=authentication&back=my-account");
			HtmlTextInput emailField = (HtmlTextInput) page.getElementById("email");
			emailField.setValueAttribute("carlos.estradaih@gmail.com");
			HtmlPasswordInput passwordField = (HtmlPasswordInput) page.getElementById("passwd");
			passwordField.setValueAttribute("Contrasenia");
			HtmlButton submitButton = (HtmlButton) page.getElementById("SubmitLogin");
			HtmlPage resultPage = submitButton.click();
			HtmlAnchor logout = (HtmlAnchor) resultPage.getAnchorByHref("http://automationpractice.com/index.php?mylogout=");
			HtmlPage respage = logout.click();
			assertEquals("Login - My Store", respage.getTitleText());
	}
}
